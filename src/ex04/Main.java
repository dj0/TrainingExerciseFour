package ex04;

public class Main {
	public static void main(String[] args) {
		BinaryTreeSearch bts = new BinaryTreeSearch();
		bts.insert(3);
		bts.insert(8);
		bts.insert(1);
		bts.insert(4);
		bts.insert(6);
		bts.insert(2);
		bts.insert(10);
		bts.insert(9);
		bts.insert(20);
		bts.insert(25);
		bts.insert(15);
		bts.insert(16);
		System.out.println("Original Tree : ");
		bts.display(bts.root);
		System.out.println("");
		System.out.println("Check whether Node with value 4 exists : " + bts.find(4));
		System.out.println("Check whether Node with value 17 exists : " + bts.find(17));
		System.out.println("Delete Node with no children (2) : " + bts.delete(2));
		bts.display(bts.root);
		System.out.println("\n Delete Node with one child (4) : " + bts.delete(4));
		bts.display(bts.root);
		System.out.println("\n Delete Node with Two children (10) : " + bts.delete(10));
		bts.display(bts.root);
	}
}

package ex04;

public class BinaryTreeSearch {
	public static Node root;

	public BinaryTreeSearch() {
		this.root = null;
	}

	public boolean find(int id) {
		Node current = root;
		while (current != null) {
			if (current.id == id) {
				return true;
			} else if (current.id > id) {
				current = current.leftNode;
			} else {
				current = current.rightNode;
			}
		}
		return false;
	}

	public boolean delete(int id) {
		Node parent = root;
		Node current = root;
		boolean isLeftChild = false;
		while (current.id != id) {
			parent = current;
			if (current.id > id) {
				isLeftChild = true;
				current = current.leftNode;
			} else {
				isLeftChild = false;
				current = current.rightNode;
			}
			if (current == null) {
				return false;
			}
		}
		
		//Case 1: if node to be deleted has no children
		if (current.leftNode == null && current.rightNode == null) {
			if (current == root) {
				root = null;
			}
			if (isLeftChild == true) {
				parent.leftNode = null;
			} else {
				parent.rightNode = null;
			}
		}
		
		//Case 2 : if node to be deleted has only one child
		else if (current.rightNode == null) {
			if (current == root) {
				root = current.leftNode;
			} else if (isLeftChild) {
				parent.leftNode = current.leftNode;
			} else {
				parent.rightNode = current.leftNode;
			}
		} else if (current.leftNode == null) {
			if (current == root) {
				root = current.rightNode;
			} else if (isLeftChild) {
				parent.leftNode = current.rightNode;
			} else {
				parent.rightNode = current.rightNode;
			}
		} else if (current.leftNode != null && current.rightNode != null) {

			
			Node successor = getSuccessor(current);
			if (current == root) {
				root = successor;
			} else if (isLeftChild) {
				parent.leftNode = successor;
			} else {
				parent.rightNode = successor;
			}
			successor.leftNode = current.leftNode;
		}
		return true;
	}

	public Node getSuccessor(Node deleteNode) {
		Node successsor = null;
		Node successsorParent = null;
		Node current = deleteNode.rightNode;
		while (current != null) {
			successsorParent = successsor;
			successsor = current;
			current = current.leftNode;
		}
		
		if (successsor != deleteNode.rightNode) {
			successsorParent.leftNode = successsor.rightNode;
			successsor.rightNode = deleteNode.rightNode;
		}
		return successsor;
	}

	public void insert(int id) {
		Node newNode = new Node(id);
		if (root == null) {
			root = newNode;
			return;
		}
		Node current = root;
		Node parent = null;
		while (true) {
			parent = current;
			if (id < current.id) {
				current = current.leftNode;
				if (current == null) {
					parent.leftNode = newNode;
					return;
				}
			} else {
				current = current.rightNode;
				if (current == null) {
					parent.rightNode = newNode;
					return;
				}
			}
		}
	}

	public void display(Node root) {
		if (root != null) {
			display(root.leftNode);
			System.out.print(" " + root.id);
			display(root.rightNode);
		}
	}
}

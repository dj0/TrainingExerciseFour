package ex04;

public class Node {
	int id;
	Node leftNode;
	Node rightNode;

	public Node(int id) {
		this.id = id;
		leftNode = null;
		rightNode = null;
	}
}

package ex01;

public class Graph {
	private int V;
	private int E;
	private boolean[][] adj;

	public Graph(int V) {
		this.V = V;
		this.E = 0;
		adj = new boolean[V][V];
		for (int v = 0; v < V; v++) {
			for (int v2 = 0; v2 < V; v2++) {
				adj[v][v2] = false;
			}
		}
	}

	public Graph() {
	}

	public void setV(int V) {
		this.V = V;
	}

	public void addVertex() {
		V++;
		boolean[][] newAdj = new boolean[V][V];
		for (int v = 0; v < V - 1; v++) {
			for (int v2 = 0; v2 < V - 1; v2++) {
				newAdj[v][v2] = adj[v][v2];
			}
		}
		adj = newAdj;
	}

	public int V() {
		return V;
	}

	public int E() {
		return E;
	}

	public boolean[][] getAdjacencyMatrix() {
		return adj;
	}

	public void addEdge(int v, int w) {
		adj[v][w] = true;
		adj[w][v] = true;
		E++;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int v = 0; v < V; v++) {
			for (int v2 = 0; v2 < V; v2++) {
				sb.append("|");
				if (adj[v][v2])
					sb.append("1");
				else
					sb.append("O");
			}
			sb.append("|");
			sb.append("\n");
		}

		return sb.toString();
	}
}

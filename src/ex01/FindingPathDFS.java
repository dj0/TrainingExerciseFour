package ex01;

import java.util.Stack;

public class FindingPathDFS {
	static Stack<Integer> stack = new Stack<>();
	boolean[][] adj;
	boolean visited[];
	int limit;
	int counter = 0;

	public void traverse(Graph graph, int beginning, int end) {
		limit = graph.V();

		visited = new boolean[limit];
		for (boolean b : visited) {
			b = false;
		}
		adj = graph.getAdjacencyMatrix();
		stack.push(beginning);
		while (!stack.isEmpty()) {
			int nodeToVisit = stack.peek();
			visit(stack.pop());
			if (nodeToVisit == end) {
				break;
			}
		}
		System.out.println("Vertices total: " + counter);

	}

	public void visit(int vertex) {
		if (visited[vertex])
			return;
		System.out.println("Now visiting " + vertex);
		counter++;
		for (int j = limit - 1; j >= 0; j--) {
			if (adj[vertex][j]) {
				stack.push(j);
			}
			visited[vertex] = true;
		}

	}
}

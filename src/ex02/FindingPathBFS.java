package ex02;

import java.util.Iterator;
import java.util.LinkedList;

public class FindingPathBFS {
	private int V;
	private LinkedList<Integer> adj[];

	FindingPathBFS(int v) {
		V = v;
		adj = new LinkedList[v];
		for (int i = 0; i < v; ++i)
			adj[i] = new LinkedList();
	}

	void addEdge(int v, int w) {
		adj[v].add(w);
	}

	void traverse(int start, int end) {
		int counter = 0;
		boolean visited[] = new boolean[V];

		LinkedList<Integer> queue = new LinkedList<Integer>();

		visited[start] = true;
		queue.add(start);

		while (queue.size() != 0) {
			start = queue.poll();
			System.out.println("Now visiting " + start);
			counter++;
			if (start == end) {
				System.out.println();
				System.out.println("Count of vertices: " + counter);
				break;
			}

			Iterator<Integer> i = adj[start].listIterator();
			while (i.hasNext()) {
				int n = i.next();
				if (!visited[n]) {
					visited[n] = true;
					queue.add(n);
				}
			}
		}
	}
}

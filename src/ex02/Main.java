package ex02;

public class Main {

	public static void main(String[] args) {
		FindingPathBFS graph = new FindingPathBFS(7);
		graph.addEdge(0, 1);
		graph.addEdge(0, 2);
		graph.addEdge(0, 4);
		graph.addEdge(1, 2);
		graph.addEdge(1, 3);
		graph.addEdge(2, 3);
		graph.addEdge(3, 5);
		graph.addEdge(3, 6);
		graph.addEdge(3, 4);
		graph.addEdge(4, 6);
		graph.addEdge(5, 6);

		graph.traverse(1, 5);
	}
}
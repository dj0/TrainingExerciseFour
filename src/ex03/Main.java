package ex03;

public class Main {

	public static void main(String[] args) {
		TestDijkstra testDijkstra = new TestDijkstra();
		testDijkstra.createNodes(11);
		testDijkstra.addLane("Edge 0", 0, 1, 66);
		testDijkstra.addLane("Edge 1", 0, 2, 127);
		testDijkstra.addLane("Edge 2", 0, 4, 213);
		testDijkstra.addLane("Edge 3", 2, 6, 96);
		testDijkstra.addLane("Edge 4", 2, 7, 233);
		testDijkstra.addLane("Edge 5", 3, 7, 73);
		testDijkstra.addLane("Edge 6", 5, 8, 150);
		testDijkstra.addLane("Edge 7", 8, 9, 184);
		testDijkstra.addLane("Edge 8", 7, 9, 269);
		testDijkstra.addLane("Edge 9", 4, 9, 322);
		testDijkstra.addLane("Edge 10", 9, 10, 140);
		testDijkstra.addLane("Edge 11", 1, 10, 410);

		testDijkstra.testExcute(4, 10);
	}

}

package ex03;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TestDijkstra {
	private List<Vertex> nodes;
	private List<Edge> edges = new ArrayList<Edge>();

	public void createNodes(int countNodes) {
		nodes = new ArrayList<Vertex>();
		for (int i = 0; i < countNodes; i++) {
			Vertex location = new Vertex("Node " + i, "Node " + i);
			nodes.add(location);
		}
	}

	public void testExcute(int start, int end) {
		try {
			Graph graph = new Graph(nodes, edges);
			DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
			dijkstra.execute(nodes.get(start));
			LinkedList<Vertex> path = dijkstra.getPath(nodes.get(end));

			int counter = 0;
			
			System.out.println("The path is: ");
			for (Vertex vertex : path) {
				System.out.println(vertex);
				counter++;
			}
			System.out.println("The distance is: " + counter);
			int sumCosts = 0;
			for (Edge edge : edges) {
				for (int i = 0; i < path.size() - 1; i++) {
					if (edge.getSource().getName().equals(path.get(i).getName())
							& edge.getDestination().getName().equals(path.get(i + 1).getName())) {
						sumCosts += edge.getWeight();
					}
				}
			}
			System.out.println("The cost is: " + sumCosts);
		} catch (NullPointerException e) {
			System.out.println("No such path!");
		} catch (IndexOutOfBoundsException e) {
			System.out.println("Graph's count is exceeded! ");
		}

	}

	public void addLane(String laneId, int sourceLocNo, int destLocNo, int duration) {
		Edge lane = new Edge(laneId, nodes.get(sourceLocNo), nodes.get(destLocNo), duration);
		edges.add(lane);
	}
}
